module fulladder(a, b, cin, sum, cout);

	input a, b, cin;
	output sum, cout;
	wire x1, a1, a2;
	xor
		xor0(x1, a, b),
		xor1(sum, x1, cin);
	and
		and0(a1, x1, cin),
		and1(a2, a, b);
	or
		or0(cout, a1, a2);
	
	
endmodule
// TODO: add a Cin
module ripple_carry_adder(A,B,Cin, S,Cout);
    input [15:0]A,B;
	 input Cin;
	 output [15:0]S;
	 output Cout;
	 
	 wire w1,w2,w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14;
	 fulladder fa_1(A[0],B[0],Cin,S[0],w1);
	 fulladder fa_2(A[1],B[1],w1,S[1],w2);
	 fulladder fa_3(A[2],B[2],w2,S[2],w3);
	 fulladder fa_4(A[3],B[3],w3,S[3],w4);
	 
	 fulladder fa_5(A[4],B[4],w4,S[4],w5);
	 fulladder fa_6(A[5],B[5],w5,S[5],w6);
	 fulladder fa_7(A[6],B[6],w6,S[6],w7);
	 fulladder fa_8(A[7],B[7],w7,S[7],w8);
	 
	 fulladder fa_9(A[8],B[8],w8,S[8],w9);
	 fulladder fa_10(A[9],B[9],w9,S[9],w10);
	 fulladder fa_11(A[10],B[10],w10,S[10],w11);
	 fulladder fa_12(A[11],B[11],w11,S[11],w12);
	 
	 fulladder fa_13(A[12],B[12],w12,S[12],w13);
	 fulladder fa_14(A[13],B[13],w13,S[13],w14);
	 fulladder fa_15(A[14],B[14],w14,S[14],w15);
	 fulladder fa_16(A[15],B[15],w15,S[15],Cout);
endmodule
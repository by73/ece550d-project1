module csa(in1, in2, ctrl, sum, overflow);

	input [31:0] in1, in2;
	input ctrl;
	output [31:0] sum;
	output overflow;
	
	wire signed_bit1, signed_bit2, invert_signed_bit2, signed_bit_sum;
	wire [15:0] b1, b2;

	// instead of using twos_complement, only invert the bits, add that 1 later on as carry in
	wire [31:0] inv_in2, res;
	generate
		genvar b;
		for (b = 0; b < 32; b = b + 1) begin: one_complement
			not ones_comp(inv_in2[b], in2[b]);
			assign res[b] = ctrl ? inv_in2[b] : in2[b];
		end
	endgenerate


	assign b1[15:0] = res[15:0];
	assign b2[15:0] = res[31:16];


	assign signed_bit1 = in1[31];
	not n0(invert_signed_bit2, in2[31]);
	assign signed_bit2 = ctrl ? invert_signed_bit2 : in2[31];
	// Do 2's complement if ctrl code is 1
	//  do the checking via mux
	
	// ripple_carry_adder(A,B,Cin, S, Cout)
	
	wire [15:0] s1,s2;
	wire c0,c1,c2;
	ripple_carry_adder rca0(in1[15:0], b1, ctrl, sum[15:0], c0);
	ripple_carry_adder rca1(in1[31:16], b2, 1'b0, s1, c1);
	ripple_carry_adder rca2(in1[31:16], b2, 1'b1, s2, c2);
	
	// select which sum to use based on actual carry out from rca0
   generate
	    genvar i; 
	    for(i=0;i<16;i=i+1) begin:changesum
 	        assign sum[i+16] = c0 ? s2[i] : s1[i];
	    end
	endgenerate

	// overflow detection	
	assign signed_bit_sum = sum[31];

	wire res1, res2, a0, a1, inv_sb1, inv_sb2, inv_sb_sum;
	not
		n1(inv_sb_sum, signed_bit_sum),
		n2(inv_sb1, signed_bit1),
		n3(inv_sb2, signed_bit2);

	// Only two cases for overflow to occur:
	//   both operands are positive, sum is negative
	//   both operands are negative, sum is positive
	and
		and0(a0, signed_bit1, signed_bit2),
		and1(res1, a0, inv_sb_sum),
		and2(a1, inv_sb1, inv_sb2),
		and3(res2, a1, signed_bit_sum);
	or
		or0(overflow, res1, res2);  	  
	
endmodule
# Project 1 Checkpoint 1
***
## Team Roster
**Bogong Yang**&nbsp;  &nbsp;  &nbsp;  netID: by73  
**Xiao Han**&nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;netID: xh114

## Design Description 

In this checkpoint, we implemented a CSA for doing addition and subtraction between two given 32-bit operands, A and B. For addition, we simply use these two operands as-is and add them using our CSA ; For  subtraction, we first perform bit-wise negation on the second operand B to obtain its one's complement form, and then use our CSA to A and B with the carry-in bit set to the value of the least significant bit of opcode, which in this case equals to 1. Finally, we detect the overflow using significant bit of both inputs and the result of the operation, based on the definition of overflow.

### 1.Ripple Carry Adder ### 
We use  `Xor`, `And`, `Or` gates to implement `Fulladder`, and then we chained 16 fulladders together to build a 16-bit RCA. We used RCA as a building block for our CSA.  

### 2.Carry select adder ###  

The CSA does two things:
* calculate the sum of the two operands;
* detect if there is an overflow for the given operation of the two operands.  

The CSA works following the steps below:  

#### Step 1 Calculate One's Complement if needed
We use `generate` to start a for loop, and then inside the loop we use `not`gate to invert each bit of the second operand, B. Then, we use the `ternary` operator to select whether the one's complement form of B or the original value of B will be used by using the least significant bit of opcode as the selector.

#### Step 2 Split both operands into two parts

We split both operands into two 16-bit numbers and feed them into the CSA.

#### Step 3 Build the CSA and finish the twos_complement
We calculate the sum of the two operands by using the CSA. Note that we use the least significant bit from opcode as the carry-in bit, such that if that bit is 1, it indicates that we are supposed to do subtraction and therefore need to add an additional 1 to the sum.
After calculating the sum, we use a for loop with ternary operators to select the correct sum calculated by the parallel part for bits 16-31 and put them together with the result from bits 0-15 to get the final sum.

#### Step 4 Detect the overflow
We first take the sign bit from both operands and the sign bit of the final sum. We name the sign bits as a, b and c accordingly. From the definition of overflow we know an overflow occurs if and only if:
1. The sum of two positive numbers is negative

2. The sum of two negative numbers is positive

Based on this definition, we come up with a Boolean function which has a truth table as follows:

| a    | b    | c    | (a & b & !c) \| ( !a & !b & c) |
| ---- | ---- | ---- | ------------------------------ |
| 0    | 0    | 0    | 0                              |
| 0    | 0    | 1    | 1                              |
| 0    | 1    | 0    | 0                              |
| 0    | 1    | 1    | 0                              |
| 1    | 0    | 0    | 0                              |
| 1    | 0    | 1    | 0                              |
| 1    | 1    | 0    | 1                              |
| 1    | 1    | 1    | 0                              |

Note that the function only evaluates to 1 iff the value of a, b and c align with the definition mentioned above.

Based on this Boolean function, we use `not`,`and`,`or` gates to implement overflow detection.  

### 3.ALU ###  

For the time being, we only have one instance of the module `Carry_select_adder`, and we directly route both operands,  `data_operandA`,`data_operandA` and the least significant bit of  `opcode` into our CSA to get the value for `data_result` and `overflow`. 
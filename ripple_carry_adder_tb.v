`timescale 1 ns / 100 ps
module ripple_carry_adder_tb();

reg signed [15:0] a;
reg [15:0] b;
reg cin;
reg clock;

wire signed [15:0] result;
wire cout, win;
ripple_carry_adder my_rca(a, b, cin, result, cout, win);
integer num_errors;

initial begin
	$display($time, " simulation start");
		
	clock = 1'b0;
	num_errors = 0;

	@(negedge clock);
	a [15:0] = 16'h8000;
	b [15:0] = 16'hf100;
	cin = 1'b0;

	@(negedge clock);
	$display("rca input a = %h, b = %h, actual sum is %h, cout is %h, win is %b", a[15:0], b[15:0], result[15:0], cout, win);
	if (result[15:0] != 16'd1) begin
		$display("incrementer case 1 failed");
		num_errors = num_errors + 1;
	end


	if (num_errors == 0) begin
		$display("Incrementer: Simulation succeeded 3 cases with no errors.");
	end else begin
		$display("Incrementer: Simulation failed with %d errors.", num_errors);
	end

end

always
	#10 clock = ~clock;

endmodule